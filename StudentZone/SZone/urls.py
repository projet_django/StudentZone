from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.lOgin, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.lOgout, name='logout'),
    path('module/', views.module, name='module'),
    path('mobile/', views.mobile, name='mobile'),
    path('web/', views.web, name='web'),
    path('db/', views.db, name='db'),
    path('hack/', views.hack, name='hack'),
    path('security/', views.security, name='security'),
    path('network/', views.network, name='network'),
    path('ytb/', views.ytb, name='ytb'),
    path('cours/', views.cours, name='cours'),

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
