from django.contrib import messages
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
import requests
from django.conf import settings
from .models import Cours
from youtubesearchpython import VideosSearch

from .models import *

connect = False,


# Create your views here.
def index(request):
    return render(request, 'SZone/home.html')


def lOgin(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # messages.success(request, 'bienvenue')
            return redirect('index')
            connect = True,
        else:
            messages.error(request, 'non')
            return redirect('login')
    return render(request, 'SZone/login.html')


def register(request):
    if request.method == "POST":
        nom = request.POST['nom']
        prenom = request.POST['prenom']
        username = request.POST['username']
        email = request.POST['email']
        university = request.POST['university']
        password = request.POST['password']
        passwordconf = request.POST['passwordconf']
        if (password == passwordconf):
            muser = User.objects.create_user(username, email, password)  # university,
            muser.save()
            mstudent = Student(nom=nom, prenom=prenom, username=username, email=email, university=university)
            mstudent.save()
            return redirect('login')
        else:
            messages.error(request, "password don't match")
            return redirect('register')
    return render(request, 'SZone/register.html')


def lOgout(request):
    logout(request)
    return redirect('index')
    connect = false,


@login_required(login_url='/login/')
def module(request):
    return render(request, 'SZone/base.html', )


def getCours(mob):
    courses = Cours.objects.filter(module=mob)
    return courses
def getcours(titre):
    cours = Cours.objects.get(titre=titre)
    return cours


def mobile(request):
    courses = getCours('mobDev')
    if request.method == 'POST':
        title = request.POST.get('title')

        course = getcours(title)
        print(course.titre)
        context={
            'titre': course.titre,
            'contenu': course.contenu
        }
        return render(request, 'SZone/courses.html',context)
    return render(request, 'SZone/modules/mobile.html', locals())


def web(request):
    courses = getCours('webDev')

    return render(request, 'SZone/modules/web.html', locals())


def db(request):
    getCours('dataAdmin')
    return render(request, 'SZone/modules/db.html')


def network(request):
    getCours('netAdmin')
    return render(request, 'SZone/modules/network.html')


def security(request):
    getCours('cybSec')
    return render(request, 'SZone/modules/security.html')


def hack(request):
    getCours('ethHack')
    return render(request, 'SZone/modules/hack.html')


def ytb(request):
    if request.method == 'GET':
        return render(request, 'SZone/ytb.html')
    elif request.method == 'POST':
        search = request.POST.get('search')
        videossearch = VideosSearch(search, limit=4)
        video = videossearch.result()
        videos = []
        for i in video['result']:
            videosdis = {
                'input': search,
                'title': i['title'],
                'id': i['id'],
                'duration': i['duration'],
                'thumbnail': i['thumbnails'][0]['url'],
                'channel': i['channel']['name'],
                'link': i['link'],
                'views': i['viewCount']['short'],
                'published': i['publishedTime']
            }
            desc = ""
            if i['descriptionSnippet']:
                for j in i['descriptionSnippet']:
                    desc += j['text']
            videosdis['description'] = desc
            videos.append(videosdis)
        print(video)
        return render(request, 'SZone/ytb.html', locals())
    else:
        messages.error(request, "error")
        return render(request, 'SZone/ytb.html', locals())


def cours(request):
    return render(request, 'SZone/courses.html', locals())
