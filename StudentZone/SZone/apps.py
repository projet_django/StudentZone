from django.apps import AppConfig


class SzoneConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'SZone'
