# Create your models here.

from django.db import models


class Student(models.Model):
    nom = models.CharField(max_length=15)
    prenom = models.CharField(max_length=15)
    username = models.CharField(max_length=20)
    email = models.EmailField()
    university = models.CharField(max_length=20)


class Cours(models.Model):
    titre = models.CharField(max_length=20)
    image = models.ImageField(upload_to='products/')
    description = models.TextField()
    module_choice = [
        ('mobDev', 'mobDev'),
        ('webDev', 'webDev'),
        ('netAdmin', 'netAdmin'),
        ('ethHack', 'ethHack'),
        ('cybSec', 'cybSec'),
        ('dataAdmin', 'dataAdmin')
    ]
    module = models.CharField(max_length=10, choices=module_choice)
    contenu = models.FileField(upload_to='pdfs/')

    class Meta:
        verbose_name = "Cours"
        verbose_name_plural = "Cours"