# Register your models here.
from django.contrib import admin

from .models import *


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['nom', 'prenom', 'username', 'email', 'university']
    search_fields = ['username']


@admin.register(Cours)
class CoursAdmin(admin.ModelAdmin):
    list_display = ['titre', 'image', 'description', 'module']
    search_fields = ['titre']
